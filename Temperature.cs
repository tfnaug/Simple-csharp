namespace ConsoleApp
{
    public abstract class Temperature 
    {
        public float unit;

        public Temperature(float unit)
        {
            this.unit = unit;
        }

        #region Formula
        public float C2F(float unit)
            => unit * 1.8f + 32f;

        public float C2K(float unit)
            => unit + 273.15f; 
        
        public float F2C(float unit)
            => (unit - 32f) * .5556f; 

        public float F2K(float unit)
            => (unit + 459.67f) * 5/9;
        
        public float K2C(float unit)
            => unit - 273.15f;

        public float K2F(float unit)
            => 9/5 * (unit - 273f) + 32f;
        #endregion

    }       
}