﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Type[] temperature = new Type[]
            {
                typeof(Fahrenheit),
                typeof(Celcius),
                typeof(Kelvin)
            };

            Console.WriteLine("FAHRENHEIT: 1");   
            Console.WriteLine("CELCIUS: 2");    
            Console.WriteLine("KELVIN: 3");
            int option;
            bool invaild;
            do
            {
                Console.Write("SELECT: ");
                option = Convert.ToInt32(Console.ReadLine()) - 1;
                invaild = option < 0 || option > 2;
                if(invaild)
                    Console.WriteLine("INVALID");
            }while(invaild);

            Console.Write("DEGREE: ");
            float unit = float.Parse(Console.ReadLine());
            IConversable convert = CreateTemperature(temperature[option], unit);
            convert.Result();

        }

        private static IConversable CreateTemperature(Type type, float unit)
            => (IConversable) Activator.CreateInstance(type, unit);
    }
}
