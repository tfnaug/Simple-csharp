namespace ConsoleApp
{
    public class Fahrenheit : Temperature, IConversable
    {
        public float inCelcius;
        public float inKelvin;

        public Fahrenheit(float unit)
            :base(unit)
        {
            inCelcius = F2C(this.unit);
            inKelvin = F2K(this.unit);
        }

        public void Result()
        {
            System.Console.WriteLine($"BASE:FAHRENHEIT : {unit}");
            System.Console.WriteLine($"CELCIUS : {inCelcius}");
            System.Console.WriteLine($"KELVIN : {inKelvin}");
        }
    }
}