namespace ConsoleApp
{
    public class Kelvin : Temperature, IConversable
    {
        public float inCelcius;
        public float inFahrenheit;

        public Kelvin(float unit)
            :base(unit)
        {
            inCelcius = K2C(this.unit);
            inFahrenheit = K2F(this.unit);
        }

        public void Result()
        {
            System.Console.WriteLine($"BASE:KELVIN : {unit}");
            System.Console.WriteLine($"CELCIUS : {inCelcius}");
            System.Console.WriteLine($"FAHRENHEIT : {inFahrenheit}");
        }
    }
}