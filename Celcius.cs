namespace ConsoleApp
{
    public class Celcius : Temperature, IConversable
    {
        public float inFahrenheit;
        public float inKelvin;

        public Celcius(float unit)
            :base(unit)
        {
            inFahrenheit = C2F(this.unit);
            inKelvin = C2K(this.unit);
        }

        public void Result()
        {
            System.Console.WriteLine($"BASE:CELCIUS : {unit}");
            System.Console.WriteLine($"FAHRENHEIT : {inFahrenheit}");
            System.Console.WriteLine($"KELVIN : {inKelvin}");
        }
    }
}